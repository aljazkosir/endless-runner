function WallContainer(scene){
    this.scene = scene;
    this.walls = [];
}

WallContainer.prototype.update = function(){
    var walls = this.scene.getWalls()
    for(var i = 0; i < walls.length; i++){
        var wall = walls[i];
        wall.update();
    }
}

WallContainer.prototype.create = function(wallDefinition){
    this.walls = wallDefinition
    for(var i = 0; i < this.walls.length; i++){
        var wallDefinition = this.walls[i];
        var material = wallDefinition.getMaterial();
        var texture = wallDefinition.getTexture();
        if(texture){
            texture.wrapS = THREE.RepeatWrapping;
            texture.wrapT = THREE.RepeatWrapping;
            texture.repeat.set( 512, 256 );
            material = new THREE.MeshBasicMaterial({map: texture});
        }
        var wall = new Wall(this.scene, wallDefinition.getGeometry(), material);
        wall.setPosition(wallDefinition.getPosition());
        wall.rotation.set(0, 0, Math.PI/2);
        this.scene.add(wall);
    }
}
