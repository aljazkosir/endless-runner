function LevelDefinition(){
  this.obstacles = [];
  this.coins = [];
  this.player = null;
  this.lights = [];
  this.surface = null;
  this.walls = [];
}

LevelDefinition.createPlayerDefinition = function(options){ //static
  var playerDefinition = new PlayerDefinition();
  playerDefinition.setSpeed(options.speed).
  setFile(options.file).
  setGeometry(options.geometry).
  setPosition(options.position).
  setTexture(options.texture).
  setMaterial(options.material);
  return playerDefinition;
}

LevelDefinition.createSurfaceDefinition = function(options){
    var surfaceDefiniton = new SurfaceDefinition();
    surfaceDefiniton.setSpeed(options.speed).
    setPosition(options.position).
    setGeometry(options.geometry).
    setTexture(options.texture).
    setMaterial(options.material);
    return surfaceDefiniton;
}

LevelDefinition.createObstacleDefinition = function(options){
    var obstacleDefinition = new ObstacleDefinition();
    obstacleDefinition.
    setGeometry(options.geometry).
    setTexture(options.texture).
    setMaterial(options.material).
    setSpawnChance(options.spawnChance);
    return obstacleDefinition;
}

LevelDefinition.createCoinDefinition = function(options){
  var coinDefinition = new CoinDefinition();
  coinDefinition.setPickUpScore(options.pickUpScore).
  setGeometry(options.geometry).
  setTexture(options.texture).
  setMaterial(options.material).
  setSpawnChance(options.spawnChance)
  return coinDefinition;
}
LevelDefinition.createLightDefinition = function(options){
  var lightDefinition = new LightDefinition();
  lightDefinition.setPosition(options.position).
  setColor(options.color).
  setLight(options.light).
  setIntensity(options.intensity);
  return lightDefinition;
}

LevelDefinition.createWallDefinition = function(options){
    var wallDefinition = new WallDefinition();
    wallDefinition.setGeometry(options.geometry).
    setTexture(options.texture).
    setMaterial(options.material).
    setPosition(options.position);
    return wallDefinition;
}

LevelDefinition.prototype.addLight = function(lightDefinition){
  this.lights.push(lightDefinition);
  return this;
}
LevelDefinition.prototype.getLights = function(){
    return this.lights;
}
LevelDefinition.prototype.addCoin = function(coinDefinition){
  this.coins.push(coinDefinition);
  return this;
}
LevelDefinition.prototype.getCoins = function(){
  return this.coins;
}
LevelDefinition.prototype.addObstacle = function(obstacleDefinition){
  this.obstacles.push(obstacleDefinition);
  return this;
}
LevelDefinition.prototype.getObstacles = function(){
    return this.obstacles;
}
LevelDefinition.prototype.setPlayer = function(playerDefinition){
  this.player = playerDefinition;
  return this;
}
LevelDefinition.prototype.getPlayer = function(){
  return this.player;
}
LevelDefinition.prototype.setSurface = function(surfaceDefinition){
    this.surface = surfaceDefinition;
    return this;
}
LevelDefinition.prototype.getSurface = function(){
    return this.surface;
}
LevelDefinition.prototype.setWall = function(wallDefinition){
    this.walls.push(wallDefinition);
    return this;
}
LevelDefinition.prototype.getWall = function(){
    return this.walls;
}
