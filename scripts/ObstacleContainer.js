function ObstacleContainer(scene){
    this.obstacles = [];
    this.scene = scene;
}
ObstacleContainer.prototype.update = function(){
    if(this.canSpawn()){
        var obstacle = this.createRandomObstacle();
        if(obstacle)
            this.scene.add(obstacle);
    }
    for(var i = 0; i < this.scene.getObstacles().length; i++){
        var obstacle = this.scene.getObstacles()[i];
        obstacle.update();
        if(obstacle.position.z > 6){
            this.scene.remove(obstacle);
        }
    }
}
ObstacleContainer.prototype.setObstacles = function(obstacleDefinition){
    this.obstacles = obstacleDefinition;
}

ObstacleContainer.prototype.canSpawn = function(){
    return Math.random() < this.scene.obstacleSpawnRate;
}

ObstacleContainer.prototype.createRandomObstacle = function(){
    var obstacleDefinition = this.getRandomObstacle();
    var material = obstacleDefinition.getMaterial();
    var texture = obstacleDefinition.getTexture();
    if(texture)
        material = new THREE.MeshBasicMaterial( { map: texture } );

    var obstacle = new Obstacle(this.scene, obstacleDefinition.getGeometry(), material);
    var position = this.scene.getSafePosition(obstacle);
    if(position){
        obstacle.setPosition(position).setSpawnChance(obstacleDefinition.getSpawnChance());
        return obstacle;
    }
}

ObstacleContainer.prototype.getRandomObstacle = function(){
    for(var i = 0; i < this.obstacles.length; i++){
        var obstacle = this.obstacles[i];
        if(Math.random() < obstacle.getSpawnChance()){
            return obstacle;
        }
    }
    return this.obstacles[0];
}
