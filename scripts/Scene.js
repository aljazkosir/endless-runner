function Scene(){
    this.score = 0;
    THREE.Scene.call(this); //needed for inheritance
    this.gravity = 0.02;
    this.coinSpawnRate = 0.03;
    this.obstacleSpawnRate = 0.023;
    this.powerUpClasses = [BulletPowerUp, ShieldPowerUp, SpeedPowerUp] //add newly added powerups
    this.spawnPosition = 80 * DEGREES_TO_RADIANS;
}
Scene.prototype = new THREE.Scene; //needed for inheritance
Scene.prototype.constructor = Scene;

Scene.prototype.setGravity = function(gravity){
    this.gravity = gravity;
    return this;
}
Scene.prototype.getGravity = function(){
    return this.gravity;
}

Scene.prototype._getObjects = function(){
    var obstacles = [];
    var players = [];
    var coins = [];
    var surfaces = [];
    var powerUps = [];
    var collidable = [];
    var bullets = [];
    var walls = [];
    for(var i = 0; i < this.children.length; i++){
        var object = this.children[i];
        if(object instanceof Obstacle){
            obstacles.push(object)
            collidable.push(object)
        }else if(object instanceof Player){
            players.push(object)
        }else if(object instanceof Coin){
            coins.push(object)
            collidable.push(object)
        }else if(object instanceof Surface){
            surfaces.push(object)
        }else if(object.name == "powerUp"){
            powerUps.push(object)
            collidable.push(object)
        }else if(object instanceof Bullet){
            bullets.push(object)
        }else if(object instanceof Wall){
            walls.push(object)
        }
  }
    return { obstacles: obstacles, players: players, coins: coins, surfaces: surfaces, powerUps: powerUps, collidable: collidable, bullets: bullets, walls: walls}
}

Scene.prototype.getCollidableObjects = function(){
    return this._getObjects().collidable;
}
Scene.prototype.getPowerUps = function(){
    return this._getObjects().powerUps;
}
Scene.prototype.getObstacles = function(){
  return this._getObjects().obstacles;
}
Scene.prototype.getPlayers = function(){
  return this._getObjects().players;
}
Scene.prototype.getCoins = function(){
  return this._getObjects().coins;
}
Scene.prototype.getSurfaces = function(){
  return this._getObjects().surfaces;
}
Scene.prototype.getBullets = function(){
    return this._getObjects().bullets;
}
Scene.prototype.getWalls = function(){
    return this._getObjects().walls;
}
Scene.prototype.getPowerUps = function(){
    return this._getObjects().powerUps;
}
Scene.prototype.addToScore = function(number){
    this.score += number;
}
Scene.prototype.getScore = function(){
    return this.score;
}
Scene.prototype.getPowerUpClasses = function(){
    return this.powerUpClasses;
}
Scene.prototype.getSafePosition = function(object){
    var position;
    var surfaceRadius = this.getSurfaces()[0].getRadius();

    position = new THREE.Vector3(getRandomInt(-3, 3), Math.sin(this.spawnPosition) * surfaceRadius - surfaceRadius, -Math.cos(this.spawnPosition) * surfaceRadius);
    object.position.x = position.x;
    object.position.y = position.y;
    object.position.z = position.z;
    var objectBoundingBox = new THREE.Box3().setFromObject(object)
    if(this.getCollidableObjects().length == 0)
        return position;

    var lastObject = this.getCollidableObjects()[this.getCollidableObjects().length - 1];
    var lastObjectBoundingBox = new THREE.Box3().setFromObject(lastObject)

    if(!objectBoundingBox.isIntersectionBox(lastObjectBoundingBox))
        return position;
}
