function WallDefinition(){
    this.position;
    this.geometry;
    this.material;
    this.texture;
}

WallDefinition.prototype.setMaterial = function(material){
    this.material = material;
    return this;
};
WallDefinition.prototype.getMaterial = function(){
    return this.material;
};
WallDefinition.prototype.setTexture = function(texture){
    this.texture = texture;
    return this;
};
WallDefinition.prototype.getTexture = function(){
    return this.texture;
};
WallDefinition.prototype.setGeometry = function(geometry){
    this.geometry = geometry;
    return this;
};
WallDefinition.prototype.getGeometry = function(){
    return this.geometry;
};
WallDefinition.prototype.getPosition = function(){
    return this.position;
};
WallDefinition.prototype.setPosition = function(position){
    this.position = position;
    return this;
};
