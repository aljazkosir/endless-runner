function PowerUpContainer(scene){
    this.scene = scene;
    this.powerUpClasses = this.scene.getPowerUpClasses();
}
PowerUpContainer.prototype.update = function(){
    this.generateNewPowerUp()
    this.updatePowerUps();
}

PowerUpContainer.prototype.spawnRandom = function(){
    var powerUp = this.getRandomPowerUp();
    var position = this.scene.getSafePosition(powerUp);
    if(position){
        powerUp.setStartingPosition(new THREE.Vector3(0, 0.5, 0))
        powerUp.setPosition(position)
        return powerUp;
    }
}
PowerUpContainer.prototype.canSpawn = function(){
    return Math.random() < 0.002;
}

PowerUpContainer.prototype.getRandomPowerUp = function(){
    var chance = Math.random();
    var chanceSum = 0;
    for(var i = 0; i < this.powerUpClasses.length; i++){
        var powerUpClass = this.powerUpClasses[i];
        chanceSum += powerUpClass.spawnChance;
    }
    var lowerChance = 0;
    for(var i = 0; i < this.powerUpClasses.length; i++){
        var powerUpClass = this.powerUpClasses[i];
        var powerUpChance = powerUpClass.spawnChance / chanceSum;
        //console.log(chance, ">=", lowerChance, "&&", chance + lowerChance, "<=", powerUpChance)
        if(chance >= lowerChance && chance <= powerUpChance + lowerChance){
            return new powerUpClass(this.scene);
        }
        lowerChance += powerUpChance;
    }
}
PowerUpContainer.prototype.generateNewPowerUp = function(){
    if(this.canSpawn()){
        var newPowerUp = this.spawnRandom();
        if(newPowerUp)
            this.scene.add(newPowerUp)
    }
}
PowerUpContainer.prototype.updatePowerUps = function(){
    var powerUps = this.scene.getPowerUps();
    for(var i = 0; i < powerUps.length; i++){
        var powerUp = powerUps[i];
        powerUp.update();
        this.removeIfOutOfrange(powerUp)
    }
}
PowerUpContainer.prototype.removeIfOutOfrange = function(powerUp){
    if(powerUp.position.z > 6){
        this.scene.remove(powerUp);
    }
}
