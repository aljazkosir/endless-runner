function BaseLight(scene){
    this.scene = scene;
    this.color;
    this.Light;
    this.intensity;
    THREE.Light.call(this)
}

BaseLight.prototype = new THREE.Light(this.color);

BaseLight.prototype.setPosition = function(position){
    this.position.x = position.x;
    this.position.y = position.y;
    this.position.z = position.z;
    return this;
}
BaseLight.prototype.setIntensity = function(intensity){
    this.intensity = intensity;
    return this;
}

BaseLight.prototype.update = function(){

}
BaseLight.prototype.setColor = function(color){
    this.color = color;
    return this;
}
BaseLight.prototype.setLight = function(Light){
    this.Light = Light;
    return this;
}
BaseLight.prototype.create = function(){
    this.light = new this.Light(this.color, this.intensity);
    this.light.position.set(this.position.x, this.position.y, this.position.z)
    return this;
}
