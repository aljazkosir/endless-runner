function PlayerContainer(scene){
  this.scene = scene;
  this.players = [];
}

PlayerContainer.prototype.update = function(){
  for(var i = 0; i < this.players.length; i++){
    var player = this.players[i];
    player.update();
  }
}

PlayerContainer.prototype.create = function(playerDefinition){
  var loader = new THREE.OBJLoader();
  var self = this;
  loader.load(playerDefinition.getFile(), function(object){
      var geometry = object.children[0].geometry;
      geometry.computeVertexNormals()
      if(!geometry){
          console.warn("Corrupted file, using default geometry")
          geometry = playerDefintion.getGeometry()
      }
      var material = playerDefinition.getMaterial();
      var texture = playerDefinition.getTexture();
      if(texture){
          material = new THREE.MeshPhongMaterial({map: texture});
          texture.wrapS = THREE.RepeatWrapping;
          texture.wrapT = THREE.RepeatWrapping;
          texture.repeat.set( 1, 1 );
      }

      geometry = playerDefinition.getGeometry();
      player = new Player(self.scene, geometry, material, 1).setSpeed(playerDefinition.getSpeed()).setPosition(playerDefinition.getPosition());
      player.setRays(object.children[0].geometry)
      var forceShield = new THREE.Mesh(new THREE.SphereGeometry(player.getRadius()*1.1, 50, 50), new THREE.MeshPhongMaterial({color: 0x2fb6c2, specular: 0x2fb6c2, shininess: 80, shading: THREE.FlatShading, transparent: true, opacity: 0.5}))
      forceShield.name = "forceShield"
      var shield = new THREE.Mesh(new THREE.SphereGeometry(player.getRadius()*1.1, 50, 50), new THREE.MeshPhongMaterial({color: 0xffcc00, transparent: true, opacity: 0.5}))
      shield.name = "shield"
      forceShield.visible = false;
      shield.visible = false;
      player.add(forceShield)
      player.add(shield)
      self.scene.add(player)
      self.players.push(player)
  });
}

PlayerContainer.prototype.getPlayer = function(){
  return this.players[0];
}
