function ObstacleDefinition(){
    this.geometry;
    this.color;
    this.spawnChance;
    this.material;
    this.texture;
}

ObstacleDefinition.prototype.setGeometry = function(geometry){
    this.geometry = geometry;
    return this;
}

ObstacleDefinition.prototype.getGeometry = function(){
    return this.geometry;
}
ObstacleDefinition.prototype.setMaterial = function(material){
    this.material = material;
    return this;
}

ObstacleDefinition.prototype.getMaterial = function(){
    return this.material;
}

ObstacleDefinition.prototype.setTexture = function(texture){
    this.texture = texture;
    return this;
}
ObstacleDefinition.prototype.getTexture = function(){
    return this.texture;
}
ObstacleDefinition.prototype.setColor = function(color){
    this.color = color;
    return this;
}

ObstacleDefinition.prototype.getColor = function(){
    return this.color;
}
ObstacleDefinition.prototype.setSpawnChance = function(spawnChance){
    this.spawnChance = spawnChance;
    return this;
}

ObstacleDefinition.prototype.getSpawnChance = function(){
    return this.spawnChance;
}
