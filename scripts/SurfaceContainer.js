function SurfaceContainer(scene){
    this.scene = scene;
    this.surfaces = [];
}

SurfaceContainer.prototype.update = function(){
    for(var i = 0; i < this.surfaces.length; i++){
        var surface = this.surfaces[i];
        surface.update();
    }
}

SurfaceContainer.prototype.create = function(surfaceDefinition){
    var material = surfaceDefinition.getMaterial();
    var texture = surfaceDefinition.getTexture();
    if(texture){
        material = new THREE.MeshBasicMaterial( { map: texture } );
        texture.wrapS = THREE.RepeatWrapping;
        texture.wrapT = THREE.RepeatWrapping;
        texture.repeat.set( 512, 1 );
    }
    var surface = new Surface(this.scene, surfaceDefinition.getGeometry(), material, 0);
    surface.setSpeed(surfaceDefinition.getSpeed()).setRotation(new THREE.Matrix4().makeRotationX(Math.PI /2 ), new THREE.Matrix4().makeRotationY(Math.PI /2 ), new THREE.Matrix4().makeRotationY(0)).
    setPosition(surfaceDefinition.getPosition())
    surface.rotation.set(0, 0, Math.PI/2);
    this.surfaces.push(surface);
    this.scene.add(surface);
}

SurfaceContainer.prototype.getSurface = function(){
    return this.surfaces[0];
}
