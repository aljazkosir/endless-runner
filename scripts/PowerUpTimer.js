function PowerUpTimer(){
 "Controls active powerups and their timers"
}
PowerUpTimer.countdowns = []
PowerUpTimer.timers = []
PowerUpTimer.update = function(scene){
    for(var key in PowerUpTimer.countdowns){
        var timer = PowerUpTimer.countdowns[key]
        timer.time --;
        PowerUpTimer._updateTimers(timer.unit)
        if(timer.time <= timer.warning){
            if(timer.lastUpdate - timer.time >= 1 * STEP){
                timer.lastUpdate = timer.time;
                PowerUpTimer._removeUnusedTimer(timer.powerup, scene)
                PowerUpTimer._addTimer(timer, scene)
            }
        }else{
            PowerUpTimer._resetTimers(timer, scene)
        }

        if(timer.time <= 0){
            PowerUpTimer._resetTimers(timer, scene)
            timer.onFinish(timer.powerup, timer.unit)
            delete PowerUpTimer.countdowns[timer.powerup.className]
        }
    }
}
PowerUpTimer.add = function(unit, powerup){
    PowerUpTimer.countdowns[powerup.className] = { time: powerup.getLife() * STEP, unit: unit, powerup: powerup, warning : powerup.getWarning() * STEP, lastUpdate: 1000 * STEP, onFinish: powerup.powerUpFinishedCallback }
}

PowerUpTimer._resetTimers = function(timer, scene){
    PowerUpTimer._removeUnusedTimer(timer.powerup, scene)
    timer.lastUpdate = 1000 * STEP;
}
PowerUpTimer.remove = function(){
    PowerUpTimer.countdowns = []
    PowerUpTimer.timers = []
}
PowerUpTimer._addTimer = function(timer, scene){
    var textMesh = PowerUpTimer._getText(timer);
    textMesh.name = timer.powerup.className + "timer";
    scene.add(textMesh)
    PowerUpTimer.timers.push(textMesh)
}

PowerUpTimer._getText = function(timer){
    var seconds = timer.time / STEP;
    var unit = timer.unit;
    var textGeom = new THREE.TextGeometry(seconds, {
        size: 0.5,
        height: 0.01,
        bevelThickness: 0.01,
        bevelSize: 0.01,
        bevelEnabled: true,
    });
    var textMesh = new THREE.Mesh(textGeom, new THREE.MeshPhongMaterial({color: timer.powerup.color, transparent: true, opacity: 0.8}))
    PowerUpTimer._updatePosition(textMesh, unit)
    return textMesh;
}
PowerUpTimer._updateTimers = function(unit){
    for(var key in PowerUpTimer.timers){
        var textMesh = PowerUpTimer.timers[key]
        PowerUpTimer._updatePosition(textMesh, unit)
    }
}
PowerUpTimer._updatePosition = function(textMesh, unit){
    var radius = unit.getRadius() * 1.1
    var bounds = textMesh.geometry.boundingSphere;
    var textRadius = 0;
    if(bounds)
        textRadius = bounds.radius
    textMesh.position.z = radius;
    textMesh.position.x = unit.position.x - textRadius;
    textMesh.position.y = unit.position.y - textRadius;
}
PowerUpTimer._removeUnusedTimer = function(powerup, scene){
    var object = scene.getObjectByName(powerup.className + "timer")
    if(object){
        scene.remove(object)
        PowerUpTimer.timers.splice(PowerUpTimer.timers.indexOf(object), 1)
    }
}
