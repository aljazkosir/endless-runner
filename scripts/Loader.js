function Loader(){
}
Loader.loadOBJ = function(path){
    var loader = new THREE.OBJLoader();
    var container = new THREE.Mesh();
    loader.load('assets/geometry/player.obj', function(object){
        mesh = new THREE.Mesh(object.children[0].geometry, new THREE.MeshLambertMaterial({color: 0xFFfff }));
        container.add(mesh)
    })
    return container;
}
