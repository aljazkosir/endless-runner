var renderer = new THREE.WebGLRenderer();
var audio = document.createElement('audio');
var source = document.createElement('source');
audio.addEventListener('ended', function() {
    this.currentTime = 0;
    //this.play();
}, false);
audio.volume = 0.1;
source.src = 'assets/music/Two_Finger_Johnny.mp3';
audio.appendChild(source);
// audio.play()

playerTexture = THREE.ImageUtils.loadTexture( 'assets/textures/football.jpg' )
surfaceTexture = THREE.ImageUtils.loadTexture( 'assets/textures/ground3.jpg' )
wallTexture = THREE.ImageUtils.loadTexture('assets/textures/plank.jpg');
obstacleTexture = THREE.ImageUtils.loadTexture( 'assets/textures/metal_texture.jpg' )
coinTexture2 = THREE.ImageUtils.loadTexture( 'assets/textures/loominarty.jpg' )

var imagePrefix = "assets/textures/skybox_";
var directions  = ["left", "right", "up", "down", "back", "front"];
var imageSuffix = ".jpg";
var skyGeometry = new THREE.BoxGeometry( 500, 500, 500 );
materialArray = [];
for (var i = 0; i < 6; i++)
    materialArray.push( new THREE.MeshBasicMaterial({
        map: THREE.ImageUtils.loadTexture( imagePrefix + directions[i] + imageSuffix ),
        side: THREE.BackSide
    }));

function startGame(){
    document.getElementById("mainMenu").style.display = "none";
    keyboard = new THREEx.KeyboardState();

    physics_stats = new Stats();
    physics_stats.domElement.style.position = 'absolute';
    physics_stats.domElement.style.top = '0px';
    physics_stats.domElement.style.zIndex = 100;
    document.getElementById( 'viewport' ).appendChild( physics_stats.domElement );
    world = new GameWorld();
    world.setLevel('a');
    world.render();

}

function showControls(){
    document.getElementById("menuItems").style.display = "none";
    document.getElementById("controlItems").style.display = "block";
}
function backToMenu(){
    document.getElementById("menuItems").style.display = "block";
    document.getElementById("controlItems").style.display = "none";
}

function playAgain(){
    document.getElementById("finalScore").style.display = "none";
    var scene = world.getScene();
    wolrd = null;
    var obj;
    for (var i = scene.children.length - 1; i >= 0 ; i -- ) {
        obj = scene.children[ i ];
        scene.remove(obj);
    }
    world = new GameWorld();
    world.setLevel('a');
    world.render();
}
