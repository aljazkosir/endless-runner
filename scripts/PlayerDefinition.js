function PlayerDefinition(){
      this.baseSpeed;
      this.position;
      this.material;
      this.geometry;
      this.file;
      this.texture;
 }

PlayerDefinition.prototype.getFile = function(){
    return this.file;
}
PlayerDefinition.prototype.setFile = function(file){
    this.file = file;
    return this;
}
PlayerDefinition.prototype.getSpeed = function(){
    return this.baseSpeed;
};
PlayerDefinition.prototype.setSpeed = function(speed){
    this.baseSpeed = speed;
    return this;
};
PlayerDefinition.prototype.setMaterial = function(material){
    this.material = material;
    return this;
};
PlayerDefinition.prototype.getMaterial = function(){
    return this.material;
};
PlayerDefinition.prototype.setTexture = function(texture){
    this.texture = texture;
    return this;
};
PlayerDefinition.prototype.getTexture = function(){
    return this.texture;
};
PlayerDefinition.prototype.getPosition = function(){
    return this.position;
};
PlayerDefinition.prototype.setPosition = function(position){
    this.position = position;
    return this;
};
PlayerDefinition.prototype.getGeometry = function(){
    return this.geometry;
};
PlayerDefinition.prototype.setGeometry = function(geometry){
    this.geometry = geometry;
    return this;
};
PlayerDefinition.prototype.getFilePath = function(){
    return this.filePath;
};
PlayerDefinition.prototype.setFilePath = function(filePath){
    this.filePath = filePath;
    return this;
};
