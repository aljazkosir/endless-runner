function ShieldPowerUp(scene){
    this.scene = scene;
    this.name = "shield"
    this.className = "ShieldPowerUp"
    this.angle = scene.spawnPosition;
    this.color = 0x2fd5b9
    Body.call(this, scene, new THREE.SphereGeometry(0.3, 64, 64), new THREE.MeshPhongMaterial({color: 0x2fb6c2, specular: 0x2fb6c2, shininess: 80, shading: THREE.FlatShading, transparent: true, opacity: 0.5}));
    this.setName("powerUp");
    this.startingPosition = new THREE.Vector3(0, 0, 0)
}

ShieldPowerUp.prototype = new Body;
ShieldPowerUp.spawnChance = 0.2;

ShieldPowerUp.prototype.update = function(){
    this.rotate(0, 0.05, 0)
    this._move();
}
ShieldPowerUp.prototype._move = function(){
    var surface = this.scene.getSurfaces()[0];
    this.angle += surface.getSpeed();
    var radius = surface.getRadius();
    var z = -Math.cos(this.angle) * radius;
    var y = Math.sin(this.angle) * radius - radius
    var position  = new THREE.Vector3(this.position.x + this.startingPosition.x, (y + this.geometry.boundingBox.max.y) + this.startingPosition.y, z)
    this.setPosition(position);
}
ShieldPowerUp.prototype.getSpawnChance = function(){
    return this.spawnChance;
}
ShieldPowerUp.prototype.onPlayerHit = function(player){
    var audio = document.createElement('audio');
    var source = document.createElement('source');
    audio.volume = 0.1;
    source.src = 'assets/fx/powerUp.wav';
    audio.appendChild(source);
    audio.play();
    this.activate(player);
}
ShieldPowerUp.prototype.onBulletHit = function(player){
    this.scene.remove(this)
}
ShieldPowerUp.prototype.activate = function(player){
    player.setImmunity(true);
    player.children[0].visible = true;
    var surface = this.scene.getSurfaces()[0]
    this.scene.remove(this);
    PowerUpTimer.add(player, this)
}
ShieldPowerUp.prototype.getLife = function(){
    return 10; //in seconds
}
ShieldPowerUp.prototype.getWarning = function(){
    return 3; //in seconds
}
ShieldPowerUp.prototype.powerUpFinishedCallback = function(self, unit){
    player.setImmunity(false)
    player.children[0].visible = false;
    var surface = self.scene.getSurfaces()[0]
}
ShieldPowerUp.prototype.setStartingPosition = function(position){
    this.startingPosition = position;
}
