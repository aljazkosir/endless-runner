function Wall(scene, geometry, material){
    this.scene = scene;
    this.geometry = geometry;
    Body.call(this, scene, geometry, material);
}
Wall.prototype = new Body;

Wall.prototype.constructor = Wall;

Wall.prototype.update = function(){
    this.rotate(this.scene.getSurfaces()[0].getSpeed(), 0, 0);
}

Wall.prototype.onPlayerHit = function(player){
    player.velocity.x = 0;
}
