function BulletContainer(scene){
    this.scene = scene;
}

BulletContainer.prototype.update = function(){
    var bullets = this.scene.getBullets()
    for(var i = 0; i < bullets.length; i++){
        var bullet = bullets[i];
        bullet.update();
    }
}
