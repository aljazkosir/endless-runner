function Coin(scene, geometry, material){
  this.scene = scene;
  this.pickUpScore;
  this.spawnChance;
  this.angle = 80 * DEGREES_TO_RADIANS;
  Body.call(this, scene, geometry, material);
  this.attraction = false;
  this.startingPosition = new THREE.Vector3(0, 0, 0)
}

Coin.prototype = new Body;

Coin.prototype.update = function(){
    this.rotate(0, 0, 0.05);
    this.move();
    this.attractToPlayer();
}
Coin.prototype.setPickUpScore = function(pickUpScore){
    this.pickUpScore = pickUpScore;
    return this;
}
Coin.prototype.setSpawnChance = function(spawnChance){
    this.spawnChance = spawnChance;
    return this;
}
Coin.prototype.onPlayerHit = function(player){
    var audio = document.createElement('audio');
    var source = document.createElement('source');
    audio.volume = 0.1;
    source.src = 'assets/fx/coin.wav';
    audio.appendChild(source);
    audio.play();
    this.scene.remove(this)
    this.scene.addToScore(this.pickUpScore)
}
Coin.prototype.onBulletHit = function(player){
    this.scene.remove(this)
}
Coin.prototype.move = function(){
    if(!this.attraction){
        var surface = this.scene.getSurfaces()[0];
        this.angle += surface.getSpeed();
        var radius = surface.getRadius();
        var z = -Math.cos(this.angle) * radius;
        var y = Math.sin(this.angle) * radius - radius
        var position  = new THREE.Vector3(this.position.x + this.startingPosition.x, y + this.geometry.boundingSphere.radius + this.startingPosition.y, z + this.startingPosition.z)
        this.setPosition(position);
    }
}
Coin.prototype.attractToPlayer = function(){
    for(var i = 0; i < this.scene.getPlayers().length; i++){
        var player = this.scene.getPlayers()[i]
        if(player.attraction){
            var distance = player.position.distanceTo(this.position)
            if(distance < 7){
                this.attraction = true;
                this.position.lerp(player.position, 0.3)
            }
        }else
            this.attraction = false;
    }
}
Coin.prototype.setStartingPosition = function(position){
    this.startingPosition = position;
}
