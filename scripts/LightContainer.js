function LightContainer(scene){
    this.lights = []
    this.scene = scene
}
LightContainer.prototype.update = function(){
    for(var i = 0; i < this.lights.length; i++){
        var light = this.lights[i];
        light.update();
    }
}
LightContainer.prototype.create = function(lightDefinition){
    var light = new BaseLight(this.scene);
    light.setPosition(lightDefinition.getPosition()).setColor(lightDefinition.getColor()).setIntensity(lightDefinition.getIntensity()).setLight(lightDefinition.getLight()).create();
    this.lights.push(light);
    this.scene.add(light.light);
}
