function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
function getGeometryType(type){
    return type.replace(/geometry/i, '').toString();
}
