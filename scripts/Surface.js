function Surface(scene, geometry, material){
    this.scene = scene;
    this.speed = 0;
    this.multiplier = 1;
    Body.call(this, scene, geometry, material);
}
Surface.prototype = new Body;
Surface.prototype.constructor = Surface;

Surface.prototype.setMultiplier = function(multiplier){
    this.multiplier = multiplier;
}

Surface.prototype.getSpeed = function(){
    return this.speed * this.multiplier;
}

Surface.prototype.setSpeed = function(speed){
    this.speed = speed;
    return this;
}

Surface.prototype.update = function(){
    this.rotate(this.speed * this.multiplier, 0, 0);
}

Surface.prototype.onPlayerHit = function(){
    console.log("Surface")
}
