function Body(scene, geometry, material){
    this.scene = scene;
    THREE.Mesh.call(this, geometry, material);
    this.geometry.computeBoundingSphere();
    this.geometry.computeBoundingBox();
}

Body.prototype = new THREE.Mesh;

Body.prototype.setPosition = function(position){
    this.position.x = position.x;
    this.position.y = position.y;
    this.position.z = position.z;
    return this;
}
Body.prototype.setRotation = function(x, y, z){
    this.rotation.x = x;
    this.rotation.y = y;
    this.rotation.z = z;
    return this;
}
Body.prototype.rotate = function(x, y, z){
    this.rotation.x += x;
    this.rotation.y += y;
    this.rotation.z += z;
    return this;
}
Body.prototype.setName = function(name){
    this.name = name;
}
Body.prototype.getRadius = function(){
    if(this.geometry.boundingSphere.radius)
        return this.geometry.boundingSphere.radius;
    else
        this.geometry.computeBoundingSphere();
    return this.geometry.boundingSphere.radius;
}
Body.prototype.getPointsFromVertices = function(geometry){
    var vertices = {}
    var points = []
    var vertexArray = geometry.attributes.position.array
    for( var i = 0; i < vertexArray.length; i+=3){
        var vertex = new THREE.Vector3(vertexArray[i], vertexArray[i + 1], vertexArray[i + 2])
        if(!(vertex.x + " " + vertex.y + " " + vertex.z in vertices))
            vertices[vertex.x + " " + vertex.y + " " + vertex.z] = vertex
        }
    for(var key in vertices){
        points.push(vertices[key])
    }
    return points;
}
