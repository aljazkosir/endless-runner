function GameWorld(){
    this.scene = new Scene();
    this.camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );
    this.camera.position.z = 5;
    this.camera.position.y = 4;
    this.camera.lookAt(new THREE.Vector3(0, 2, 0));
    renderer.shadowMapEnabled = true;
    renderer.shadowMapSoft = true;
    renderer.setSize( window.innerWidth, window.innerHeight );
    renderer.setClearColor( 0xFFF0B6, 1);
    document.body.removeChild(document.body.lastElementChild)
    document.body.appendChild( renderer.domElement );
    this.addSkyBox();
    this.playerContainer = new PlayerContainer(this.scene);
    this.surfaceContainer = new SurfaceContainer(this.scene);
    this.wallContainer = new WallContainer(this.scene);
    this.coinContainer = new CoinContainer(this.scene);
    this.lightContainer = new LightContainer(this.scene);
    this.obstacleContainer = new ObstacleContainer(this.scene);
    this.powerUpCointainer = new PowerUpContainer(this.scene)
    this.bulletContainer = new BulletContainer(this.scene)
    this.initEventListeners();
    this.isRunning = true;
}

GameWorld.prototype.initEventListeners = function(){
    var self = this;
    var event = new Event('gameOver');
    window.addEventListener('resize', function(){self.onWindowResize()}, false);
    window.addEventListener('gameOver', function(e){self.onGameOver(), e.target.removeEventListener(e.type, arguments.callee)}, false)
}

GameWorld.prototype.setLevel = function(id){
    var levelData = new LevelData();
    var levelDefinition = levelData.getLevelById(id);
    this.playerContainer.create(levelDefinition.getPlayer());
    this.surfaceContainer.create(levelDefinition.getSurface());
    this.wallContainer.create(levelDefinition.getWall());
    this.coinContainer.setCoins(levelDefinition.getCoins());
    this.obstacleContainer.setObstacles(levelDefinition.getObstacles());
    for(var i = 0; i < levelDefinition.getLights().length; i++){
      this.lightContainer.create(levelDefinition.getLights()[i])
    }
}
GameWorld.prototype.update = function(){
    document.getElementById("score").innerHTML = "Score: " + this.scene.getScore();
    physics_stats.update();
    this.handleCameraMovement();
    this.playerContainer.update();
    this.surfaceContainer.update();
    this.wallContainer.update();
    this.coinContainer.update();
    this.lightContainer.update();
    this.obstacleContainer.update();
    this.powerUpCointainer.update();
    this.bulletContainer.update();
    PowerUpTimer.update(this.scene)
}
GameWorld.prototype.addSkyBox = function(){
    var skyMaterial = new THREE.MeshFaceMaterial( materialArray );
    var skyBox = new THREE.Mesh( skyGeometry, skyMaterial );
    this.scene.add( skyBox );
}

GameWorld.prototype.render = function() {
    if(this.isRunning){
        this.update()
        renderer.render( this.scene, this.camera );
        window.requestAnimationFrame(this.render.bind(this))
    }
}

GameWorld.prototype.handleCameraMovement = function(){
    if(keyboard.pressed("W"))
        this.camera.position.z --;
    if(keyboard.pressed("S"))
        this.camera.position.z ++;
    if(keyboard.pressed("A"))
        this.camera.position.x --;
    if(keyboard.pressed("D"))
        this.camera.position.x ++;
    if(keyboard.pressed("E"))
        this.camera.position.y --;
    if(keyboard.pressed("Q"))
        this.camera.position.y ++;

    if(this.camera.position.x >= 4.3)
        this.camera.position.x = 4.3
    if(this.camera.position.x <= -4.3)
        this.camera.position.x = -4.3
    if(this.camera.position.y <= 1)
        this.camera.position.y = 1;
    if(this.camera.position.y >= 8)
        this.camera.position.y = 8;
    if(this.camera.position.z >= 9)
        this.camera.position.z = 9;
    if(this.camera.position.z <= 3)
        this.camera.position.z = 3;

}
GameWorld.prototype.onGameOver = function(){
    var self = this;
    this.isRunning = false;
    PowerUpTimer.remove();
    var gameOver = new GameOver(this.scene, this)
    delete this;
}

GameWorld.prototype.onWindowResize = function() {
    console.log("wut")
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();

    renderer.setSize( window.innerWidth, window.innerHeight );
}
GameWorld.prototype.getScene = function(){
    return this.scene;
}
