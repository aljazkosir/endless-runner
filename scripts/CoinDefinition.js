function CoinDefinition(){
  this.pickUpScore;
  this.texture;
  this.material;
  this.spawnChance;
  this.geometry;
}

CoinDefinition.prototype.setPickUpScore = function(pickUpScore){
  this.pickUpScore = pickUpScore;
  return this;
}

CoinDefinition.prototype.getPickUpScore = function(){
  return this.pickUpScore;
}

CoinDefinition.prototype.setGeometry = function(geometry){
    this.geometry = geometry;
    return this;
}

CoinDefinition.prototype.getGeometry = function(){
    return this.geometry;
}

CoinDefinition.prototype.setTexture = function(texture){
    this.texture = texture;
    return this;
}

CoinDefinition.prototype.getTexture = function(){
    return this.texture;
}

CoinDefinition.prototype.setMaterial = function(material){
  this.material = material;
  return this;
}

CoinDefinition.prototype.getMaterial = function(){
  return this.material;
}
CoinDefinition.prototype.setSpawnChance = function(spawnChance){
  this.spawnChance = spawnChance;
  return this;
}

CoinDefinition.prototype.getSpawnChance = function(){
  return this.spawnChance;
}
