function CoinContainer(scene){
  this.coins = [];
  this.scene = scene;
}
CoinContainer.prototype.update = function(){
    this.spawnCoin();
    for(var i = 0; i < this.scene.getCoins().length; i++){
        var coin = this.scene.getCoins()[i]
        coin.update();
        if(coin.position.z > 6){
            this.scene.remove(coin);
        }
    }
}
CoinContainer.prototype.spawnCoin = function(){
    if(this.canSpawn()){
        var coin = this.createRandomCoin();
        if(coin)
            this.scene.add(coin);
    }
}
CoinContainer.prototype.setCoins = function(coinsDefinition){
  this.coins = coinsDefinition;
}
CoinContainer.prototype.canSpawn = function(){
  return Math.random() < this.scene.coinSpawnRate;
}
CoinContainer.prototype.createRandomCoin = function(){
    var coinDefinition = this.getRandomCoin();
    var material = coinDefinition.getMaterial();
    var texture = coinDefinition.getTexture();
    if(texture)
        material = new THREE.MeshBasicMaterial( { map: texture } );

    var coin = new Coin(this.scene, coinDefinition.getGeometry(), material);
    var position = this.scene.getSafePosition(coin);
    if(position){
        coin.setPickUpScore(coinDefinition.getPickUpScore()).
        setPosition(position).
        setSpawnChance(coinDefinition.getSpawnChance()).
        setRotation(Math.PI/2, 0, 0);
        return coin;
    }
}
CoinContainer.prototype.getRandomCoin = function(){
    for(var i = 0; i < this.coins.length; i++){
        var coin = this.coins[i];
        if(Math.random() < coin.getSpawnChance()){
            return coin;
        }
    }
    return this.coins[0];
}
