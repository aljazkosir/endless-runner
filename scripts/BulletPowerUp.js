function BulletPowerUp(scene){
    this.scene = scene;
    this.name = "bullet"
    this.className = "BulletPowerUp"
    this.color = 0x6f12ec;
    Body.call(this, scene, new THREE.SphereGeometry(0.3, 64, 64), new THREE.MeshPhongMaterial({ambient: 0x292929, color: 0x292929, specular: 0x494949, shininess: 80, shading: THREE.FlatShading }));
    this.setName("powerUp");
    this.angle = scene.spawnPosition;
    this.startingPosition = new THREE.Vector3(0, 0, 0)
}

BulletPowerUp.prototype = new Body;
BulletPowerUp.spawnChance = 0.1;

BulletPowerUp.prototype.update = function(){
    this.rotate(0, 0.05, 0)
    this._move();
}
BulletPowerUp.prototype._move = function(){
    var surface = this.scene.getSurfaces()[0];
    this.angle += surface.getSpeed();
    var radius = surface.getRadius();
    var z = -Math.cos(this.angle) * radius;
    var y = Math.sin(this.angle) * radius - radius
    var position  = new THREE.Vector3(this.position.x + this.startingPosition.x, (y + this.geometry.boundingBox.max.y) + this.startingPosition.y, z)
    this.setPosition(position);
}
BulletPowerUp.prototype.getSpawnChance = function(){
    return this.spawnChance;
}
BulletPowerUp.prototype.onPlayerHit = function(player){
    var audio = document.createElement('audio');
    var source = document.createElement('source');
    audio.volume = 0.1;
    source.src = 'assets/fx/powerUp.wav';
    audio.appendChild(source);
    audio.play();
    this.activate(player);
}
BulletPowerUp.prototype.onBulletHit = function(player){
    this.scene.remove(this)
}
BulletPowerUp.prototype.activate = function(player){
    player.bulletCount ++;
    this.scene.remove(this);
}
BulletPowerUp.prototype.setStartingPosition = function(position){
    this.startingPosition = position;
}
