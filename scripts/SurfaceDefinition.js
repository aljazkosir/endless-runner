function SurfaceDefinition(){
    this.baseSpeed;
    this.position;
    this.color;
    this.material;
    this.geometry;
    this.texture;
}

SurfaceDefinition.prototype.getSpeed = function(){
    return this.baseSpeed;
};
SurfaceDefinition.prototype.setSpeed = function(speed){
    this.baseSpeed = speed;
    return this;
};
SurfaceDefinition.prototype.setMaterial = function(material){
    this.material = material;
    return this;
};
SurfaceDefinition.prototype.getMaterial = function(){
    return this.material;
};
SurfaceDefinition.prototype.setTexture = function(texture){
    this.texture = texture;
    return this;
};
SurfaceDefinition.prototype.getTexture = function(){
    return this.texture;
};
SurfaceDefinition.prototype.setGeometry = function(geometry){
    this.geometry = geometry;
    return this;
};
SurfaceDefinition.prototype.getGeometry = function(){
    return this.geometry;
};
SurfaceDefinition.prototype.getPosition = function(){
    return this.position;
};
SurfaceDefinition.prototype.setPosition = function(position){
    this.position = position;
    return this;
};
