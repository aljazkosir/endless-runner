function SpeedPowerUp(scene){
    this.scene = scene;
    this.name = "speed"
    this.className = "SpeedPowerUp"
    this.angle = scene.spawnPosition;
    this.color = 0xaec500;
    Body.call(this, scene, new THREE.SphereGeometry(0.3, 64, 64), new THREE.MeshPhongMaterial({ambient: 0x378d09, color: 0x378d09, specular: 0x54D60E, shininess: 80, shading: THREE.FlatShading, transparent: true, opacity: 0.9 }));
    this.setName("powerUp");
    this.startingPosition = new THREE.Vector3(0, 0, 0)
}

SpeedPowerUp.prototype = new Body;
SpeedPowerUp.spawnChance = 0.1;

SpeedPowerUp.prototype.update = function(){
    this.rotate(0, 0.05, 0)
    this._move();
}
SpeedPowerUp.prototype._move = function(){
    var surface = this.scene.getSurfaces()[0];
    this.angle += surface.getSpeed();
    var radius = surface.getRadius();
    var z = -Math.cos(this.angle) * radius;
    var y = Math.sin(this.angle) * radius - radius
    var position  = new THREE.Vector3(this.position.x + this.startingPosition.x, (y + this.geometry.boundingBox.max.y) + this.startingPosition.y, z)
    this.setPosition(position);
}
SpeedPowerUp.prototype.getSpawnChance = function(){
    return this.spawnChance;
}
SpeedPowerUp.prototype.onPlayerHit = function(player){
    var audio = document.createElement('audio');
    var source = document.createElement('source');
    audio.volume = 0.1;
    source.src = 'assets/fx/powerUp.wav';
    audio.appendChild(source);
    audio.play();
    this.activate(player);
}
SpeedPowerUp.prototype.onBulletHit = function(player){
    this.scene.remove(this)
}
SpeedPowerUp.prototype.activate = function(player){
    var multiplier = 2;
    this.scene.coinSpawnRate = 0.06;
    this.scene.obstacleSpawnRate = 0.046;
    player.setAttraction(true);
    var surface = this.scene.getSurfaces()[0]
    surface.setMultiplier(multiplier);
    this.scene.remove(this);
    PowerUpTimer.add(player, this)
}
SpeedPowerUp.prototype.getLife = function(){
    return 10; //in seconds
}
SpeedPowerUp.prototype.getWarning = function(){
    return 3; //in seconds
}
SpeedPowerUp.prototype.powerUpFinishedCallback = function(self, unit){
    var multiplier = 2;
    self.scene.coinSpawnRate = 0.03;
    self.scene.obstacleSpawnRate = 0.023;
    player.setAttraction(false)
    var surface = self.scene.getSurfaces()[0]
    surface.setMultiplier(1);
}
SpeedPowerUp.prototype.setStartingPosition = function(position){
    this.startingPosition = position;
}
