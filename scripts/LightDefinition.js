function LightDefinition(){
  this.color;
  this.position;
  this.light;
  this.intensity;
}

LightDefinition.prototype.setPosition = function(position){
  this.position = position;
  return this;
}

LightDefinition.prototype.getPosition = function(){
  return this.position;
}

LightDefinition.prototype.setColor = function(color){
  this.color = color;
  return this;
}

LightDefinition.prototype.getColor = function(){
  return this.color;
}
LightDefinition.prototype.setIntensity = function(intensity){
  this.intensity = intensity;
  return this;
}

LightDefinition.prototype.getIntensity = function(){
  return this.intensity;
}

LightDefinition.prototype.setLight = function(light){
  this.light = light;
  return this;
}

LightDefinition.prototype.getLight = function(){
  return this.light;
}
