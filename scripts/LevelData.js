function LevelData(){
  this.levels = [];
  this.levels['a'] = this.getLevel();
}

LevelData.prototype.getLevel = function(){
  var surfaceOffset = new THREE.Matrix4();
  surfaceOffset.makeTranslation(0, 5, 0);
  var surfaceFence = new THREE.CylinderGeometry(504, 504, 1, 1024);
  var surfaceGeometry = new THREE.CylinderGeometry(500, 500, 9, 1024);
  surfaceGeometry.merge(surfaceFence, surfaceOffset);
  surfaceOffset.makeTranslation(0, -5, 0);
  surfaceGeometry.merge(surfaceFence, surfaceOffset);


  var obstacleOffset = new THREE.Matrix4();
  obstacleOffset.makeTranslation(0, 0.75, 0);

  var obstacleGeometry = [];
  for(i = 0; i < 5; i++){
      obstacleGeometry[i] = new THREE.BoxGeometry(i+1, 1, 1);
      obstacleGeometry[i+10] = new THREE.BoxGeometry(i+1, 2, 1);
  }
  for(i = 5; i < 10; i++){
      obstacleGeometry[i] = new THREE.BoxGeometry(i-4, 1, 1);
      obstacleGeometry[i].applyMatrix(obstacleOffset);
      obstacleGeometry[i+10] = new THREE.BoxGeometry(i-4, 2, 1);
      obstacleGeometry[i+10].applyMatrix(obstacleOffset);
  }

  var level = new LevelDefinition();
  level.setPlayer(LevelDefinition.createPlayerDefinition({file: 'assets/geometry/player.obj', geometry: new THREE.SphereGeometry(1, 50, 50), position: new THREE.Vector3(2, 1, 0), speed: 0.01, texture: playerTexture, material: new THREE.MeshPhongMaterial({ ambient: 0xffffff, color: 0xffffff, specular: 0x8E2800, shininess: 150, shading: THREE.FlatShading })})).
  setSurface(LevelDefinition.createSurfaceDefinition({geometry: new THREE.CylinderGeometry(500, 500, 9, 1024), position: new THREE.Vector3(0, -500, 0), speed: 0.03 * DEGREES_TO_RADIANS, texture: surfaceTexture, material: new THREE.MeshPhongMaterial({color: 0x468966})})).
  addCoin(LevelDefinition.createCoinDefinition({pickUpScore: 1, geometry: new THREE.CylinderGeometry(0.5, 0.5, 0.1, 32), material: new THREE.MeshPhongMaterial({ambient: 0x030303, color: 0xFFD700, specular: 0xFFD700, shininess: 200, shading: THREE.FlatShading }), spawnChance: 0.5})).
  addCoin(LevelDefinition.createCoinDefinition({pickUpScore: 5, geometry: new THREE.CylinderGeometry(0.5, 0.5, 0.1, 32), material: new THREE.MeshPhongMaterial({ambient: 0x030303, color: 0xe52c1f, specular: 0xe52c1f, shininess: 200, shading: THREE.FlatShading}), spawnChance: 0.1})).
  addLight(LevelDefinition.createLightDefinition({position: new THREE.Vector3(-1, 1, 2), light: THREE.DirectionalLight, color: 0xffffff, intensity: 0.5})).
  addLight(LevelDefinition.createLightDefinition({position: new THREE.Vector3(1, 1, 2), light: THREE.DirectionalLight, color: 0xffffff, intensity: 0.5})).
  setWall(LevelDefinition.createWallDefinition({geometry: new THREE.CylinderGeometry(504, 504, 1, 4096), position: new THREE.Vector3(-5, -500, 0), texture: wallTexture, material: new THREE.MeshPhongMaterial({color: 0x468966})})).
  setWall(LevelDefinition.createWallDefinition({geometry: new THREE.CylinderGeometry(504, 504, 1, 4096), position: new THREE.Vector3(5, -500, 0), texture: wallTexture, material: new THREE.MeshPhongMaterial({color: 0x468966})}));

  for(i = 0; i < obstacleGeometry.length; i++){
      level.addObstacle(LevelDefinition.createObstacleDefinition({geometry: obstacleGeometry[i], texture: obstacleTexture, material: new THREE.MeshPhongMaterial({color: 0x8A2919}), spawnChance: 0.1}));
  }

  return level;
}

LevelData.prototype.getLevelById = function(id){
  return this.levels[id];
}
