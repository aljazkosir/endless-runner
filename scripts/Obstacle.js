function Obstacle(scene, geometry, material){
    this.scene = scene;
    this.color;
    this.spawnChance;
    this.angle = scene.spawnPosition;
    Body.call(this, scene, geometry, material);
    this.startingPosition = new THREE.Vector3(0, 0, 0)
}

Obstacle.prototype = new Body;
Obstacle.prototype.constructor = Obstacle;

Obstacle.prototype.update = function(){
    var surface = this.scene.getSurfaces()[0];
    this.angle += surface.getSpeed();
    var radius = surface.getRadius();
    var z = -Math.cos(this.angle) * radius;
    var y = Math.sin(this.angle) * radius - radius;
    var position  = new THREE.Vector3(this.position.x + this.startingPosition.x, (y + this.geometry.boundingBox.max.y) + this.startingPosition.y, z)
    this.setPosition(position);
}

Obstacle.prototype.setSpawnChance = function(spawnChance){
    this.spawnChance = spawnChance;
    return this;
}
Obstacle.prototype.onBulletHit = function(player){
    this.scene.remove(this)
}

Obstacle.prototype.onPlayerHit = function(player){
    if(!player.getImmunity()){
        window.dispatchEvent(new Event('gameOver'))
    }
}

Obstacle.prototype.setStartingPosition = function(position){
    this.startingPosition = position;
}
