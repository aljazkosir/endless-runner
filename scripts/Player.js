function Player(scene, geometry, material, mass){
  this.scene = scene;
  this.speed = null;
  PhysicsBody.call(this, scene, geometry, material, mass);
  this.jumpIsReady = true;
  this.canShrink = true;
  this.rays = []
  this.caster = new THREE.Raycaster();
  this.immunity = false;
  this.attraction = false;
  this.ignoredCollisions = []
  this.bulletCount = 5;
  this.canFireBullet = true;
  this.collisionEnabled = true;
}
Player.prototype = new PhysicsBody;
Player.prototype.constructor = Player;

Player.prototype.setSpeed = function(speed){
    this.speed = speed;
    return this;
}

Player.prototype.update = function(){
    document.getElementById("bullet").innerHTML = "Bullet count: " + this.bulletCount;
    this.Update()
    if(this.collisionEnabled)
        this.handleCollision();
    this.handleWallCollision();
    this.processKeyboardInput();
    this.clearing();
}

Player.prototype.processKeyboardInput = function(){
    this.handleMovement();
    this.handleShrinking();
    this.handleFiringBullets();
    if(keyboard.pressed("C"))
        this.collisionEnabled = false;
}
Player.prototype.handleFiringBullets = function(){
    if(keyboard.pressed("space") && this.bulletCount > 0 && this.canFireBullet){
        this.canFireBullet = false;
        createjs.Tween.get(this).to({canFireBullet: true}, 200)
        this.bulletCount --;
        var bullet = new Bullet(this.scene);
        var startPosition = this.position.clone();
        startPosition.y *= 0.8;
        bullet.setStartingPosition(startPosition);
        this.scene.add(bullet)
    }
}
Player.prototype.handleShrinking = function(){
    if(keyboard.pressed("down") && this.canShrink){
        this.canShrink = false;
        createjs.Tween.get(this.geometry.boundingSphere).to({radius: 0.5}, 100).wait(400).to({radius: 1}, 100)
        createjs.Tween.get(this.scale).to({x: 0.5, y: 0.5, z: 0.5}, 100).wait(400).to({x: 1, y: 1, z: 1}, 100).call(this.shrinkFinished, [], this)
    }
}

Player.prototype.shrinkFinished = function(){
    this.canShrink = true;
}

Player.prototype.handleMovement = function(){
    if(keyboard.pressed("right"))
        this.applyImpulse(new THREE.Vector3(this.speed, 0, 0));
    if(keyboard.pressed("left"))
        this.applyImpulse(new THREE.Vector3(-this.speed, 0, 0));
    if(keyboard.pressed("up") && this.onGround)
        this.applyImpulse(new THREE.Vector3(0, this.speed * 40, 0))
}

Player.prototype.handleWallCollision = function(){
    var sphereRadius = this.getRadius();
    if(this.position.x < -4.5 + sphereRadius){
        this.velocity.x = 0;
        this.position.x = -4.5 + sphereRadius;
    }
    else if(this.position.x > 4.5 - sphereRadius){
        this.velocity.x = 0;
        this.position.x = 4.5 - sphereRadius;
    }
}

Player.prototype.handleCollision = function(){
    var distance = this.geometry.boundingSphere.radius
    var collisions;
    var collidableObjects = this.scene.getCollidableObjects()
    for(var i = 0; i < this.rays.length; i++){
        this.caster.set(this.position, this.rays[i])
        collisions = this.caster.intersectObjects(collidableObjects)
        for(var j = 0; j < collisions.length; j++){
            var potentialCollision = collisions[j]
            if(potentialCollision.distance <= distance && this.ignoredCollisions.indexOf(collisions[j].object.id) == -1){
                var collidedWith = collisions[j].object
                collidedWith.onPlayerHit(this);
                this.ignoredCollisions.push(collidedWith.id)
            }
        }
    }
}
Player.prototype.getImmunity = function(){
    return this.immunity;
}
Player.prototype.setImmunity = function(immunity){
    this.immunity = immunity;
    return this;
}
Player.prototype.getAttraction = function(){
    return this.attraction;
}
Player.prototype.setAttraction = function(attraction){
    this.attraction = attraction;
    return this;
}

Player.prototype.clearing = function(){
    for(var i = 0; i < this.ignoredCollisions.length; i++){
        var id = this.ignoredCollisions[i]
        if(!this.scene.getObjectById(id))
            this.ignoredCollisions.splice(i, 1)
    }
}

Player.prototype.setRays = function(geometry){
    this.rays = this.getPointsFromVertices(geometry);
}
