function Bullet(scene){
    this.scene = scene;
    this.speed = 0;
    this.geometry = new THREE.SphereGeometry(0.2, 32, 32);
    this.material = new THREE.MeshPhongMaterial({ambient: 0x313131, color: 0x292929, specular: 0x494949, shininess: 80, shading: THREE.FlatShading })
    Body.call(this, scene, this.geometry, this.material);
    this.angle = 90 * DEGREES_TO_RADIANS;
    this.startingPosition;
}
Bullet.prototype = new Body;
Bullet.prototype.constructor = Bullet;


Bullet.prototype.getSpeed = function(){
    return this.speed;
}
Bullet.prototype.setSpeed = function(speed){
    this.speed = speed;
    return this;
}
Bullet.prototype.setStartingPosition = function(position){
    this.startingPosition = position;
    return this;
}
Bullet.prototype.update = function(){
    this.handleCollisions();
    var surface = this.scene.getSurfaces()[0];
    this.angle += surface.getSpeed() * 2;
    var radius = surface.getRadius();
    var z = Math.cos(this.angle) * radius;
    var y = Math.sin(this.angle) * radius - radius
    var position  = new THREE.Vector3(this.startingPosition.x, (y + this.geometry.boundingSphere.radius) + this.startingPosition.y, z)
    this.setPosition(position);
}
Bullet.prototype.handleCollisions = function(){
    var bulletBox = new THREE.Box3().setFromObject(this)
    var collidableObjects = this.scene.getCollidableObjects()
    for(var i = 0; i < collidableObjects.length; i++){
        var object = collidableObjects[i]
        var objectBox = new THREE.Box3().setFromObject(object)
        if(bulletBox.isIntersectionBox(objectBox) || bulletBox.containsBox(objectBox)){
            object.onBulletHit(this);
            this.scene.remove(this)
        }
    }
}
