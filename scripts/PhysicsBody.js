function PhysicsBody(scene, geometry, material, mass){ //basic movement physics
    Body.call(this, scene, geometry, material)
    this.mass = mass;
    this.scene = scene;
    this.velocity = new THREE.Vector3(0, 0, 0)
    this.onGround = false;
}

PhysicsBody.prototype = new Body;
PhysicsBody.prototype.constructor = PhysicsBody;


PhysicsBody.prototype.applyImpulse = function(impulse){
    this.velocity.x += impulse.x;
    this.velocity.y += impulse.y;
    this.velocity.z += impulse.z;
}

PhysicsBody.prototype.Update = function(){
    var surface = this.scene.getSurfaces()[0]
    this.onGround = false;
    this.velocity.y -= this.scene.getGravity() * this.mass
    this.position.y += this.velocity.y;
    this.position.x += this.velocity.x;
    this.rotation.z -= this.velocity.x;
    this.rotation.x -= surface.getSpeed() * RADIANS_TO_DEGREES*4;
    if(this.position.y < this.getRadius()){
        this.position.y = this.getRadius();
        this.onGround = true;
        this.velocity.y = 0;
    }
}
